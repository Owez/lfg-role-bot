# DFG Bot

## About

"DFG Bot" is an easy-to-use whitelist bot made on a clients request.

## Setup

1. Install Python 3.6 and the [`pipenv` package](https://docs.pipenv.org/en/latest/).
2. Enter the directory where this `README.md` is located.
3. Run in the terminal `pipenv install` to install all of the dependencies for this project and create a virtual enviroment to safely run inside.
4. Enter the virtual environment with `pipenv shell`.
5. Once you are inside of the virtual environment, you can set the bot's token using an environment variable. On Linux, this would look like: `export TOKEN=[bot token]` and on Windows, it would look something like: `set TOKEN=[bot token]`.
6. Create a special message in it's own channel and react with the unicode emote you'd like.
7. Open the `config.toml` file in the same directory as this README and get ready to edit it.
8. "Copy ID" of the channel the special message in and add it to the afformentioned `config.toml` file where it says **`channel_for_message`**.
9. "Copy ID" of the announcement/special message and paste it into the `config.toml` file where it says **`message_to_react`**.
10. Change the prefix to your desire in the configuration file (`config.toml`). The "prefix" is the prefix you'll be using with the bot. For example, you can try the ping command when the bot is running by doing `!ping`. In this case the bot prefix is `!`.
11. If you would like to customize the "whitelist name" (the user-facing name of the team/role they have just been granted access to), it is stored in the config file as `role_name`.
12. You will find the CLI (command-line-interface) by running `python app.py` inside of the newly-created virtual environment. The next step will show you how to start the bot using this CLI.
13. After you have a look around the CLI, you can run the bot by executing `python app.py start-bot`.
14. That's it! The bot should be up and running.

## Usage

Once the bot is online by following the [setup](#setup) tutorial, you can find the documentation of commands by doing `!help` in a discord server where the bot is added to.

## Optional configuration options

- `access_time`: Allows configuration of the time that users have access to the role and in turn, the channel. **Default: 60 mins**.
