import os
import click
from dfg_bot import DFGBot
from dfg_bot.utils import Config


@click.group()
def base_group():
    """
    A CLI frontend to easily install/run a redis server and run the discord bot.
    """

    pass


@click.command()
def start_bot():
    """
    Starts the bot. Make sure you have redis running properly beforehand.
    """

    config = Config("config.toml")
    DFGBot(config).run(config.CLIENT_TOKEN)


base_group.add_command(start_bot)

if __name__ == "__main__":
    base_group()
