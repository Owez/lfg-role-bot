from discord.ext import commands
from dfg_bot.utils import embed_generator


class Ping(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(
        name="ping",
        description="A debug ping command to easily test that the bot is online",
    )
    async def ping_command(self, ctx):
        RETURN_EMOTE = "🏓"

        await ctx.send(embed=embed_generator({"main": {"Ping": RETURN_EMOTE}}))


def setup(client):
    """
    Adds the command to the client object
    """

    client.add_cog(Ping(client))
    print("\tLoaded Ping cog!")
