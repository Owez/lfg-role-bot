from discord.ext import commands
from dfg_bot.utils import Config, embed_generator


class Help(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.config = Config("config.toml")

    @commands.command(
        name="help",
        description="Gets help for commands and actions the bot can preform",
    )
    async def help_command(self, ctx):
        help_payload = {
            "main": {
                "`!ping`": "A ping command to check if the client is indeed online",
                "`!help`": "The command you are currently viewing",
                "Roles": (
                    "Add a reaction to the special message in "
                    f"<#{self.config.CHANNEL}> to be added to "
                    f"{self.config.ROLE_NAME} (<@&{self.config.ROLE}>)."
                ),
            }
        }
        await ctx.send(embed=embed_generator(help_payload))


def setup(client):
    """
    Adds the command to the client object
    """

    client.add_cog(Help(client))
    print("\tLoaded Help cog!")
