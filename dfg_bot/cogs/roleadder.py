import asyncio
from discord.ext import commands
from dfg_bot.utils import Config, embed_generator, get_react_message


class RoleAdder(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.config = Config("config.toml")

    async def _add_timer(self, user, role_to_remove):
        """
        Adds a 1 hour timer to remove role.

        - `reaction_message`: The Discord.Message object that gained a reaction
        - `user`: The Discord.User object that added an emote and got a role
        - `emote_used`: The emote from `on_raw_reaction_(add/remove).emoji`
        - `role_to_remove`: The role to remove from the user at the end of the
        1 hour waiting period.
        """

        await asyncio.sleep(
            60 * self.config.ACCESS_TIME
        )  # Wait for the time used in `ACCESS_TIME`
        await user.remove_roles(role_to_remove)  # Remove user role

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, reaction_payload):
        """
        Gets the un-cached reaction when one is removed from a message. The
        role of this block is to unsubscribe a user from the role if they click
        on the right message.
        """

        found_guild = self.client.get_guild(reaction_payload.guild_id)
        found_user = await found_guild.fetch_member(reaction_payload.user_id)

        react_message = await get_react_message(found_guild, self.config)
        react_role = found_guild.get_role(self.config.ROLE)

        if (
            reaction_payload.message_id == self.config.MESSAGE
            and react_role in found_user.roles
        ):
            await found_user.remove_roles(react_role)

            success_payload = {
                "main": {
                    f"Removed from {self.config.ROLE_NAME}": (
                        f"You have been removed from '{self.config.ROLE_NAME}' "
                        "as you unsubscribed."
                    )
                }
            }
            await found_user.send(embed=embed_generator(success_payload))

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, reaction_payload):
        """
        Gets reaction data and user that posted it. If on right message and
        is the right emote for said role (taken from config.toml), add the user
        to role
        """

        found_guild = self.client.get_guild(reaction_payload.guild_id)
        found_user = await found_guild.fetch_member(reaction_payload.user_id)

        react_message = await get_react_message(found_guild, self.config)
        react_role = found_guild.get_role(self.config.ROLE)

        if react_role in found_user.roles:
            join_dupe_payload = {
                "main": {
                    f"Already in {self.config.ROLE_NAME}!": (
                        f"You are already subscribed to {self.config.ROLE_NAME} "
                        "so you will not be added again."
                    )
                }
            }
            await found_user.send(embed=embed_generator(join_dupe_payload))
            return

        if reaction_payload.message_id == self.config.MESSAGE:
            if reaction_payload.emoji.name == self.config.EMOTE:
                await found_user.add_roles(react_role)

                dm_payload = {
                    "main": {
                        f"Welcome to {self.config.ROLE_NAME}!": (
                            "You have been added to our whitelist role and "
                            f"are now able to access the {self.config.ROLE_NAME} "
                            "channel!"
                        )
                    }
                }
                await found_user.send(embed=embed_generator(dm_payload))
                await self._add_timer(found_user, react_role)
            else:
                failure_payload = {
                    "main": {
                        "Unrecognised emote": (
                            f"The given emote '{reaction_payload.emoji}' was not "
                            "recognised for the message this bot is viewing! "
                            "Please try again using the set reaction:"
                            f"'{self.config.EMOTE}'."
                        )
                    }
                }
                await found_user.send(embed=embed_generator(failure_payload))

            await react_message.remove_reaction(
                reaction_payload.emoji, found_user
            )


def setup(client):
    """
    Adds the command to the client object
    """

    client.add_cog(RoleAdder(client))
    print("\tLoaded RoleAdder cog!")
