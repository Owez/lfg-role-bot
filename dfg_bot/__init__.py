from discord.ext import commands
from dfg_bot.utils import Config, get_cogs


class DFGBot(commands.Bot):
    def __init__(self, config: Config):
        super().__init__(command_prefix=config.PREFIX)

        self.remove_command("help")

    async def on_ready(self):
        print(f"'{self.user.name}' is online with the id '{self.user.id}'!")

        for cog in get_cogs():
            self.load_extension(cog)
